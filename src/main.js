import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import 'element-ui/lib/theme-chalk/index.css'

import App from './App'

import router from './router'
import Element from 'element-ui'
import locale from 'element-ui/lib/locale/lang/en'
Vue.use(Element, { locale })

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
